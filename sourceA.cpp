#include "sqlite3.h"
#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>

using namespace std;
int main()
{
	int rc;
	sqlite3* db;
	char *zErrMsg = 0;
	bool flag = true;

	rc = sqlite3_open("FirstPart.db", &db);

	if (rc)
	{
		cout << "Can't open database: " << sqlite3_errmsg(db) << endl;
		sqlite3_close(db);
		system("Pause");
		return(1);
	}

	system("CLS");

	sqlite3_exec(db, "CREATE TABLE people(peopleID INT key autoincrement, peopleName VARCHAR", 0, 0, &zErrMsg);
	sqlite3_exec(db, "insert into people(peopleName) values ('Dani')", 0, 0, &zErrMsg);
	sqlite3_exec(db, "insert into people(peopleName) values ('Levy')", 0, 0, &zErrMsg);
	sqlite3_exec(db, "insert into people(peopleName) values ('Sara')", 0, 0, &zErrMsg);
	sqlite3_exec(db, "UPDATE people SET peoplName='Hcana' WHERE peopleID=3", 0, 0, &zErrMsg);




	sqlite3_close(db);
	system("Pause");
	return(1);


}